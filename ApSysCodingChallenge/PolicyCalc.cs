﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApSysCodingChallenge
{
    
    class PolicyCalc
    {
        /**
         * Method to check that policy start date is valid
         * (i.e. not before current date).
         * Returns true if valid.
         */
        public static bool ValidStart(DateTime date)
        {
            bool isValid = true;
            DateTime today = DateTime.Today;

            if (date.Date < today)
                isValid = false;

            return isValid;
        }

        /**
         * Method to check if customer age is within acceptable range.
         * Returns int value dependent on age entered.
         * 1 = Under 17
         * 2 = 17 - 25
         * 3 = 26 - 75
         * 4 = Over 75
         */
        public static int AgeCheck(DateTime dob, DateTime startDate)
        {
            int ageCode = 0;
            int age = startDate.Year - dob.Year;
            if (dob.AddYears(age).Date > startDate.Date)
                age--;

            if (age < 17)
                ageCode = 1;
            else if(age <= 25)
                ageCode = 2;
            else if (age <= 75)
                ageCode = 3;
            else
                ageCode = 4;

            return ageCode;
        }

        /**
         * Method to calculate final premium ammount based upon
         * customers' age and occupation.
         * Returns total premium amount as a double.
         * Between 17 and 25 - add 20%
         * Between 26 and 75 - subract 10%
         * Occupation = Chauffeur - add 10%
         * Occupation = Accountant - subtract 10%
         */
        public static double PremiumCalc(int ageCode, String occupation)
        {
            double basePremium = 500.00;
            double premiumRate = 1.0;
            double premium;

            if (ageCode == 2)
                premiumRate += 0.2;
            else
                premiumRate -= 0.1;

            if (occupation.Equals("CHAUFFEUR"))
            {
                premiumRate += 0.1;
            }
            else if (occupation.Equals("ACCOUNTANT"))
            {
                premiumRate -= 0.1;
            }

            premium = basePremium * premiumRate;

            return premium;
        }

    }

    
}
