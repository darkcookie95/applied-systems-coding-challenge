﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApSysCodingChallenge
{
    public partial class Decision : Form
    {
        private int decisionCode;
        private String occupation;

        public Decision()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        /**
         * Initilises page and stores the passed value need to
         * correctly make decision.
         */
        public Decision(int decisionCode, String occupation) : this()
        {
            this.decisionCode = decisionCode;
            this.occupation = occupation;
        }

        /**
         * Makes decision to accept or decline policy dependent on
         * passed value and displays appropriate message to user.
         */
        private void DisplayDecision()
        {
            switch (decisionCode)
            {
                case 1: Decline("Driver is too young");
                    break;
                case 2:
                case 3: Accept();
                    break;
                case 4: Decline("Driver is too old");
                    break;
                case 5: Decline("Start Date of Policy cannot be in the past");
                    break;
            }
        }

        /**
         * Declines policy with relevent reason.
         */
        private void Decline(string reason)
        {
            lblDeclined.Visible = true;
            lblMessage.Text = reason;
            lblMessage.Visible = true;
        }

        /**
         * Accepts policy and displays appropriate premium ammount.
         */
        private void Accept()
        {
            double premium = PolicyCalc.PremiumCalc(decisionCode, occupation);
            lblAccepted.Visible = true;
            lblMessage.Text = "Premium Amount: £" + premium;
            lblMessage.Visible = true;
        }

        private void BtnHome_Click(object sender, EventArgs e)
        {
            Program.home.Show();

            Program.policy.Close();
            this.Close();
        }

        private void Decision_Load(object sender, EventArgs e)
        {
            DisplayDecision();
        }
    }
}
