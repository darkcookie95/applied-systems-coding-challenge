﻿namespace ApSysCodingChallenge
{
    partial class Policy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpPolicyStart = new System.Windows.Forms.DateTimePicker();
            this.lblPolicyStart = new System.Windows.Forms.Label();
            this.lblDetails = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.lblOccupation = new System.Windows.Forms.Label();
            this.lblDOB = new System.Windows.Forms.Label();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.txtName = new System.Windows.Forms.TextBox();
            this.txtOccupation = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnProceed = new System.Windows.Forms.Button();
            this.lblNameReq = new System.Windows.Forms.Label();
            this.lblOccupationReq = new System.Windows.Forms.Label();
            this.lblStartReq = new System.Windows.Forms.Label();
            this.lblDOBReq = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // dtpPolicyStart
            // 
            this.dtpPolicyStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPolicyStart.Location = new System.Drawing.Point(424, 149);
            this.dtpPolicyStart.Name = "dtpPolicyStart";
            this.dtpPolicyStart.Size = new System.Drawing.Size(200, 24);
            this.dtpPolicyStart.TabIndex = 0;
            // 
            // lblPolicyStart
            // 
            this.lblPolicyStart.AutoSize = true;
            this.lblPolicyStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPolicyStart.Location = new System.Drawing.Point(171, 154);
            this.lblPolicyStart.Name = "lblPolicyStart";
            this.lblPolicyStart.Size = new System.Drawing.Size(135, 18);
            this.lblPolicyStart.TabIndex = 1;
            this.lblPolicyStart.Text = "Policy Start Date";
            // 
            // lblDetails
            // 
            this.lblDetails.AutoSize = true;
            this.lblDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDetails.Location = new System.Drawing.Point(110, 223);
            this.lblDetails.Name = "lblDetails";
            this.lblDetails.Size = new System.Drawing.Size(110, 18);
            this.lblDetails.TabIndex = 2;
            this.lblDetails.Text = "Driver Details";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(254, 257);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(52, 18);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Name";
            // 
            // lblOccupation
            // 
            this.lblOccupation.AutoSize = true;
            this.lblOccupation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOccupation.Location = new System.Drawing.Point(207, 298);
            this.lblOccupation.Name = "lblOccupation";
            this.lblOccupation.Size = new System.Drawing.Size(99, 18);
            this.lblOccupation.TabIndex = 4;
            this.lblOccupation.Text = "Occuptation";
            // 
            // lblDOB
            // 
            this.lblDOB.AutoSize = true;
            this.lblDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOB.Location = new System.Drawing.Point(201, 342);
            this.lblDOB.Name = "lblDOB";
            this.lblDOB.Size = new System.Drawing.Size(103, 18);
            this.lblDOB.TabIndex = 5;
            this.lblDOB.Text = "Date of Birth";
            // 
            // dtpDOB
            // 
            this.dtpDOB.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDOB.Location = new System.Drawing.Point(424, 336);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(200, 24);
            this.dtpDOB.TabIndex = 6;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(424, 257);
            this.txtName.MaxLength = 255;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(200, 20);
            this.txtName.TabIndex = 7;
            // 
            // txtOccupation
            // 
            this.txtOccupation.Location = new System.Drawing.Point(424, 299);
            this.txtOccupation.Name = "txtOccupation";
            this.txtOccupation.Size = new System.Drawing.Size(200, 20);
            this.txtOccupation.TabIndex = 8;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(204, 469);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(167, 57);
            this.btnExit.TabIndex = 9;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // btnProceed
            // 
            this.btnProceed.Location = new System.Drawing.Point(446, 469);
            this.btnProceed.Name = "btnProceed";
            this.btnProceed.Size = new System.Drawing.Size(167, 57);
            this.btnProceed.TabIndex = 10;
            this.btnProceed.Text = "Proceed";
            this.btnProceed.UseVisualStyleBackColor = true;
            this.btnProceed.Click += new System.EventHandler(this.BtnProceed_Click);
            // 
            // lblNameReq
            // 
            this.lblNameReq.AutoSize = true;
            this.lblNameReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameReq.ForeColor = System.Drawing.Color.Red;
            this.lblNameReq.Location = new System.Drawing.Point(630, 259);
            this.lblNameReq.Name = "lblNameReq";
            this.lblNameReq.Size = new System.Drawing.Size(82, 18);
            this.lblNameReq.TabIndex = 11;
            this.lblNameReq.Text = "*Required";
            // 
            // lblOccupationReq
            // 
            this.lblOccupationReq.AutoSize = true;
            this.lblOccupationReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOccupationReq.ForeColor = System.Drawing.Color.Red;
            this.lblOccupationReq.Location = new System.Drawing.Point(630, 301);
            this.lblOccupationReq.Name = "lblOccupationReq";
            this.lblOccupationReq.Size = new System.Drawing.Size(82, 18);
            this.lblOccupationReq.TabIndex = 12;
            this.lblOccupationReq.Text = "*Required";
            // 
            // lblStartReq
            // 
            this.lblStartReq.AutoSize = true;
            this.lblStartReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStartReq.ForeColor = System.Drawing.Color.Red;
            this.lblStartReq.Location = new System.Drawing.Point(630, 155);
            this.lblStartReq.Name = "lblStartReq";
            this.lblStartReq.Size = new System.Drawing.Size(82, 18);
            this.lblStartReq.TabIndex = 13;
            this.lblStartReq.Text = "*Required";
            // 
            // lblDOBReq
            // 
            this.lblDOBReq.AutoSize = true;
            this.lblDOBReq.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDOBReq.ForeColor = System.Drawing.Color.Red;
            this.lblDOBReq.Location = new System.Drawing.Point(630, 341);
            this.lblDOBReq.Name = "lblDOBReq";
            this.lblDOBReq.Size = new System.Drawing.Size(82, 18);
            this.lblDOBReq.TabIndex = 14;
            this.lblDOBReq.Text = "*Required";
            // 
            // Policy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 556);
            this.Controls.Add(this.lblDOBReq);
            this.Controls.Add(this.lblStartReq);
            this.Controls.Add(this.lblOccupationReq);
            this.Controls.Add(this.lblNameReq);
            this.Controls.Add(this.btnProceed);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.txtOccupation);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.dtpDOB);
            this.Controls.Add(this.lblDOB);
            this.Controls.Add(this.lblOccupation);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblDetails);
            this.Controls.Add(this.lblPolicyStart);
            this.Controls.Add(this.dtpPolicyStart);
            this.Name = "Policy";
            this.Text = "Policy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpPolicyStart;
        private System.Windows.Forms.Label lblPolicyStart;
        private System.Windows.Forms.Label lblDetails;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblOccupation;
        private System.Windows.Forms.Label lblDOB;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.TextBox txtOccupation;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnProceed;
        private System.Windows.Forms.Label lblNameReq;
        private System.Windows.Forms.Label lblOccupationReq;
        private System.Windows.Forms.Label lblStartReq;
        private System.Windows.Forms.Label lblDOBReq;
    }
}