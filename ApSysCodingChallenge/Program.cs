﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApSysCodingChallenge
{
    static class Program
    {


        public static Home home;
        public static Policy policy;
        public static Decision decision;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(home = new Home());
            
        }

        
    }
}
