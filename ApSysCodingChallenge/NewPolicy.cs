﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApSysCodingChallenge
{
    public partial class Policy : Form
    {
        public Policy()
        {
            InitializeComponent();
            this.StartPosition = FormStartPosition.CenterScreen;
        }

        private void BtnProceed_Click(object sender, EventArgs e)
        {
            /**
             * Verifies that all fields have been completed before proceeding.
             * If ValidStart returns true, checks customer age and passes ageCode
             * to Decision page. If ValidStart returns false, skips AgeCheck and 
             * passes 5 to Decision page.
             */
            if (txtName.TextLength > 0 && txtOccupation.TextLength > 0)
            {
                if (PolicyCalc.ValidStart(dtpPolicyStart.Value))
                {
                    int ageCode = PolicyCalc.AgeCheck(dtpDOB.Value, dtpPolicyStart.Value);
                    String occupation = txtOccupation.Text.ToUpper();
                    Program.decision = new Decision(ageCode, occupation);
                    Program.decision.Visible = true;
                }
                else
                {
                    Program.decision = new Decision(5, null);
                    Program.decision.Visible = true;
                }
            }
      
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
